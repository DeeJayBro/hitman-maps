<?php

namespace BusinessLogic;


abstract class MissionType {
    const MISSION = "Mission";
    const BONUS_EPISODE = "Bonus Episode";
    const PATIENT_ZERO = "Patient Zero";
    const ELUSIVE_TARGET = "Elusive Target";
}
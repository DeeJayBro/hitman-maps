<?php

namespace Controllers\ViewModels;


class DisguiseViewModel {
    public $id;
    public $name;
    public $image;
    public $areas = [];
}